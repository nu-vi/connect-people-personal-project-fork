package org.caxineirosdegema.connectingpeople.DTO.Converters;

import org.caxineirosdegema.connectingpeople.DTO.EventDTO;
import org.caxineirosdegema.connectingpeople.persistence.model.Event;
import org.caxineirosdegema.connectingpeople.services.EventService;
import org.caxineirosdegema.connectingpeople.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class DtoToEvent implements Converter<EventDTO, Event> {

    private EventService eventService;
    private UserService userService;

    @Autowired
    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public Event convert(EventDTO eventDTO) {

        Event event =(eventDTO.getId() != null ? eventService.get(eventDTO.getId()) : new Event());

        event.setTitle(eventDTO.getTitle());
        event.setDescription(eventDTO.getDescription());
        event.setLocation(eventDTO.getLocation());
        //event.setPublicAccess(eventDTO.isPublicAccess());
        //event.setOwner(userService.get(eventDTO.getOwnerDto().getId()));
        event.setDay(eventDTO.getDay());
        event.setMonth(eventDTO.getMonth());
        event.setDate(eventDTO.getDate());
        event.setTime(eventDTO.getTime());
        event.setNumberOfPeople(eventDTO.getNumberOfPeople());

        return event;
    }
}
