package org.caxineirosdegema.connectingpeople.DTO.Converters;

import org.caxineirosdegema.connectingpeople.DTO.UserDTO;
import org.caxineirosdegema.connectingpeople.persistence.model.User;
import org.caxineirosdegema.connectingpeople.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;

@Component
public class DtoToUser implements Converter<UserDTO, User> {

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public User convert(UserDTO userDTO) {

        User user = (userDTO.getId() != null ? userService.get(userDTO.getId()) : new User());

        user.setName(userDTO.getName() != null ? userDTO.getName() : user.getName());
        user.setPassword(userDTO.getPassword() != null ? userDTO.getPassword() : user.getPassword());
        user.setEmail(userDTO.getEmail() != null ? userDTO.getEmail() : user.getEmail());
        user.setCity(userDTO.getCity() != null ? userDTO.getCity() : user.getCity());
        user.setPhone(userDTO.getPhone() != null ? userDTO.getPhone() : user.getPhone());

        return user;
    }
}
