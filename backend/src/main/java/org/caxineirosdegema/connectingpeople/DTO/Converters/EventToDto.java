package org.caxineirosdegema.connectingpeople.DTO.Converters;


import org.caxineirosdegema.connectingpeople.DTO.EventDTO;
import org.caxineirosdegema.connectingpeople.persistence.model.Event;
import org.springframework.stereotype.Component;

@Component
public class EventToDto extends AbstractConverter<Event, EventDTO> {

    private UserToDto userToDto = new UserToDto();

    @Override
    public EventDTO convert(Event event) {

       EventDTO eventDTO = new EventDTO();

       eventDTO.setId(event.getId());
       eventDTO.setTitle(event.getTitle());
       eventDTO.setDescription(event.getDescription());
       eventDTO.setLocation(event.getLocation());
       eventDTO.setPublicAccess(event.isPublicAccess());
       eventDTO.setOwnerDto(userToDto.convert(event.getOwner()));
       eventDTO.setDay(event.getDay());
       eventDTO.setMonth(event.getMonth());
       eventDTO.setDate(event.getDate());
       eventDTO.setTime(event.getTime());
       eventDTO.setNumberOfPeople(event.getNumberOfPeople());

       return eventDTO;
    }
}
