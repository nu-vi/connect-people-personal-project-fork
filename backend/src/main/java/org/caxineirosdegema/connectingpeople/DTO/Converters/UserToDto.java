package org.caxineirosdegema.connectingpeople.DTO.Converters;

import org.caxineirosdegema.connectingpeople.DTO.UserDTO;
import org.caxineirosdegema.connectingpeople.persistence.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserToDto extends AbstractConverter<User, UserDTO> {

    @Override
    public UserDTO convert(User user) {

        UserDTO userDTO = new UserDTO();

        userDTO.setId(user.getId());
        userDTO.setName(user.getName());
        userDTO.setPassword(user.getPassword());
        userDTO.setEmail(user.getEmail());
        userDTO.setCity(user.getCity());
        userDTO.setPhone(user.getPhone());

        return userDTO;
    }
}
