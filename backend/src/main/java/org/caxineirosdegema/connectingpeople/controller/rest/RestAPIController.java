package org.caxineirosdegema.connectingpeople.controller.rest;

import org.caxineirosdegema.connectingpeople.DTO.Converters.DtoToEvent;
import org.caxineirosdegema.connectingpeople.DTO.Converters.DtoToUser;
import org.caxineirosdegema.connectingpeople.DTO.Converters.EventToDto;
import org.caxineirosdegema.connectingpeople.DTO.Converters.UserToDto;
import org.caxineirosdegema.connectingpeople.DTO.EventDTO;
import org.caxineirosdegema.connectingpeople.DTO.UserDTO;
import org.caxineirosdegema.connectingpeople.persistence.model.Event;
import org.caxineirosdegema.connectingpeople.persistence.model.User;
import org.caxineirosdegema.connectingpeople.services.AuthenticationService;
import org.caxineirosdegema.connectingpeople.services.EventService;
import org.caxineirosdegema.connectingpeople.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class RestAPIController {

    private AuthenticationService applicationService;
    private UserService userService;
    private EventService eventService;
    private EventToDto eventToDto;
    private DtoToEvent dtoToEvent;
    private UserToDto userToDto;
    private DtoToUser dtoToUser;

    @RequestMapping(method = RequestMethod.POST, path= "/login")
    public ResponseEntity<UserDTO> login(@Valid @RequestBody UserDTO userDTO, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        UserDTO loggedUser = userToDto.convert(applicationService.authenticateUser(userDTO.getEmail(), userDTO.getPassword()));

        if(loggedUser != null ) {
            return new ResponseEntity<>(loggedUser, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }


    @RequestMapping(method = RequestMethod.GET, path = "/user/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDTO> getUser(@PathVariable Integer id) {

        if(userService.get(id) == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        UserDTO userDTO = userToDto.convert(userService.get(id));

        return new ResponseEntity<>(userDTO, HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.POST, path = "/create-user")
    public ResponseEntity createUser(@Valid @RequestBody UserDTO userDTO, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        User user = dtoToUser.convert(userDTO);

        userService.save(user);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @RequestMapping(method = {RequestMethod.GET, RequestMethod.PUT}, path = "/edit-user/{id}")
    public ResponseEntity editUser(@Valid @RequestBody UserDTO userDTO, BindingResult bindingResult, @PathVariable Integer id) {

        if(bindingResult.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        if(userDTO.getId() != null && !userDTO.getId().equals(id)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        if(userService.get(id) == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        userDTO.setId(id);

        userService.save(dtoToUser.convert((userDTO)));
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, path="/delete-user/{id}")
    public ResponseEntity deleteUser(@PathVariable Integer id) {

        if(userService.get(id) == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        userService.delete(id);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(method = {RequestMethod.GET, RequestMethod.POST}, path="/user/{id}/add-friend")
    public ResponseEntity addFriend(@Valid @RequestBody UserDTO userDTO, BindingResult bindingResult, @PathVariable Integer id) {

        if (bindingResult.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Integer fid = userService.get(userDTO.getEmail()).getId();

        if(userService.get(id) == null || userService.get(fid) == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        userService.addFriend(id, fid);
        return new ResponseEntity<>(HttpStatus.OK);


       /* User u = userService.get(id);

        Set<User> userSet = applicationService.getUserSet();

        for (User toFind : userSet) {
            if (user.getEmail().equals(toFind.getEmail())) {
                u.getFriendsList().add(toFind);
                Set<Event> eventSet = toFind.getEventSet();

                for (Event event: eventSet) {
                    u.getEventSet().add(event);
                }

                eventSet.addAll(u.getEventSet());

                return new ResponseEntity<>(HttpStatus.OK);
            }

        }

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);*/
    }

    @RequestMapping(method = RequestMethod.GET, path = "/user/{id}/own-event-set", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Set<EventDTO>> getUserOwnEvents(@PathVariable Integer id) {

        User user = userService.get(id);

        if(user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Set<EventDTO> eventSetDTO = user
                .getOwnEventList()
                .stream()
                .map(event -> eventToDto.convert(event))
                .collect(Collectors.toSet());

        return new ResponseEntity<>(eventSetDTO, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/user/{id}/event-set", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<EventDTO>> getUserEvents(@PathVariable Integer id) {

        User user = userService.get(id);

        if(user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        List<EventDTO> eventSetDTO = user
                .getEventList()
                .stream()
                .map(event -> eventToDto.convert(event))
                .collect(Collectors.toList());

        return new ResponseEntity<>(eventSetDTO, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/user/{id}/friends", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<UserDTO>> getUserFriends(@PathVariable Integer id) {

        User user = userService.get(id);

        if(user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        List<UserDTO> userListDTO = user
                .getFriendsList()
                .stream()
                .map(friend -> userToDto.convert(friend))
                .collect(Collectors.toList());

        return new ResponseEntity<>(userListDTO, HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.GET, path = "/user/{uid}/event/{eid}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EventDTO> getEvent(@PathVariable Integer uid, @PathVariable Integer eid) {

        User user = userService.get(uid);
        Event event = eventService.get(eid);

        if(event == null || user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        EventDTO eventDTO = eventToDto.convert(event);

        return new ResponseEntity<>(eventDTO, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/user/{uid}/create-event")
    public ResponseEntity<UserDTO> createEvent(@Valid @RequestBody EventDTO eventDTO, BindingResult bindingResult, @PathVariable Integer uid) {

        if(bindingResult.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        User user = userService.get(uid);

        if(user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Event event = dtoToEvent.convert(eventDTO);

        userService.addEvent(uid, event);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }


    @RequestMapping(method = RequestMethod.POST, path = "/user/{uid}/edit-event/{eid}")
    private ResponseEntity editEvent(@PathVariable Integer uid, @PathVariable Integer eid, @Valid @RequestBody Event event, BindingResult bindingResult) {

        if(bindingResult.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        if(eventService.save(event) != null) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(method = RequestMethod.DELETE, path="/user/{uid}/delete-event/{eid}")
    public ResponseEntity deleteEvent(@PathVariable Integer uid, @PathVariable Integer eid) {

        userService.removeEvent(uid, eid);

        return new ResponseEntity<>(HttpStatus.OK);

    }

    @Autowired
    public void setApplicationService(AuthenticationService applicationService) {
        this.applicationService = applicationService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }

    @Autowired
    public void setEventToDto(EventToDto eventToDto) {
        this.eventToDto = eventToDto;
    }

    @Autowired
    public void setDtoToEvent(DtoToEvent dtoToEvent) {
        this.dtoToEvent = dtoToEvent;
    }

    @Autowired
    public void setUserToDto(UserToDto userToDto) {
        this.userToDto = userToDto;
    }

    @Autowired
    public void setDtoToUser(DtoToUser dtoToUser) {
        this.dtoToUser = dtoToUser;
    }
}
