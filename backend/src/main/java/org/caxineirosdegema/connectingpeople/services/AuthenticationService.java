package org.caxineirosdegema.connectingpeople.services;

import org.caxineirosdegema.connectingpeople.persistence.model.User;

import java.util.Set;

public interface AuthenticationService {

    User authenticateUser(String email, String password);

}
