package org.caxineirosdegema.connectingpeople.services;

import org.caxineirosdegema.connectingpeople.persistence.model.Event;
import org.caxineirosdegema.connectingpeople.persistence.model.User;

public interface EventService {

    Event get(Integer id);

    Event save(Event event);

}
