package org.caxineirosdegema.connectingpeople.services;

import org.caxineirosdegema.connectingpeople.persistence.dao.EventDao;
import org.caxineirosdegema.connectingpeople.persistence.dao.UserDao;
import org.caxineirosdegema.connectingpeople.persistence.model.Event;
import org.caxineirosdegema.connectingpeople.persistence.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@Service
public class UserServiceIMPL implements UserService {

    private UserDao userDao;
    private EventDao eventDao;

    @Autowired
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    @Autowired
    public void setEventDao(EventDao eventDao) {
        this.eventDao = eventDao;
    }

    @Override
    public User get(Integer id) {
        return userDao.findById(id);
    }

    @Transactional(readOnly = true)
    @Override
    public User get(String email) {

        List<User> userList = userDao.findAll();

        for(User user : userList) {
            if(user.getEmail().equals(email))
                return user;
        }
        return null;
    }

    @Transactional
    @Override
    public User save(User user) {
        return userDao.saveOrUpdate(user);
    }

    @Transactional
    @Override
    public void delete(Integer id) {
        userDao.delete(id);
    }

    @Override
    public List<User> list() {
        return userDao.findAll();
    }

    @Override
    public Set<User> listFriends(Integer id) {
        User user = userDao.findById(id);

        return user.getFriendsList();
    }

    @Override
    public Set< Event> listEvents(Integer id) {
        User user = userDao.findById(id);

        return user.getOwnEventList();
    }

    @Transactional
    @Override
    public Event addEvent(Integer id, Event event) {
        User user = userDao.findById(id);
        Set<User> friends = user.getFriendsList();

        user.addEvent(event);

        userDao.saveOrUpdate(user);

        return null;
        //return user.getOwnEventList().get(user.getOwnEventList().size() - 1);
    }

    @Transactional
    @Override
    public void removeEvent(Integer id, Integer eventId) {
        User user = userDao.findById(id);
        Event event = eventDao.findById(eventId);

        user.removeEvent(event);
        userDao.saveOrUpdate(user);
    }

    @Transactional
    @Override
    public void addFriend(Integer userId, Integer friendId) {
        User user = userDao.findById(userId);
        User friend = userDao.findById(friendId);

        user.addFriend(friend);

        for(Event event : friend.getOwnEventList()) {
            user.getEventList().add(event);
        }

        userDao.saveOrUpdate(user);
        userDao.saveOrUpdate(friend);

        //user.getFriendsList().hashCode().get(user.getFriendsList().size() - 1);
    }

    @Transactional
    @Override
    public void removeFriend(Integer userId, Integer friendId) {
        User user = userDao.findById(userId);
        User friend = userDao.findById(friendId);

        user.removeFriend(friend);
        userDao.saveOrUpdate(user);
        userDao.saveOrUpdate(friend);
    }
}


