package org.caxineirosdegema.connectingpeople.services;

import org.caxineirosdegema.connectingpeople.persistence.dao.UserDao;
import org.caxineirosdegema.connectingpeople.persistence.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AuthenticationServiceIMPL implements AuthenticationService {

    private UserDao userDao;

    @Autowired
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    @Transactional (readOnly = true)
    @Override
    public User authenticateUser(String email, String password) {

        List<User> userList = userDao.findAll();

        for(User user : userList) {
            if(user.getEmail().equals(email) && user.getPassword().equals(password)) {
                return user;
            }
        }
        return null;
    }
}
