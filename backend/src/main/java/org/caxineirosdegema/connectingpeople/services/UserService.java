package org.caxineirosdegema.connectingpeople.services;

import org.caxineirosdegema.connectingpeople.persistence.model.Event;
import org.caxineirosdegema.connectingpeople.persistence.model.User;

import java.util.List;
import java.util.Set;

public interface UserService {

    User get(Integer id);

    User get(String email);

    User save(User user);

    void delete(Integer id);

    List<User> list();

    Set<User> listFriends(Integer id);

    Set<Event> listEvents(Integer id);

    Event addEvent(Integer id, Event event);

    void removeEvent(Integer id, Integer eventId);

    void addFriend(Integer userId, Integer friendId);

    void removeFriend(Integer userId, Integer friendId);

}
