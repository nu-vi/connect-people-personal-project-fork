package org.caxineirosdegema.connectingpeople.services;


import org.caxineirosdegema.connectingpeople.persistence.dao.EventDao;
import org.caxineirosdegema.connectingpeople.persistence.model.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@Service
public class EventServiceIMPL implements EventService {

    private EventDao eventDao;

    @Autowired
    public void setEventDao(EventDao eventDao) {
        this.eventDao = eventDao;
    }

    @Override
    public Event get(Integer id) {
        return eventDao.findById(id);
    }

    @Transactional
    @Override
    public Event save(Event event) {
        return eventDao.saveOrUpdate(event);
    }
}

