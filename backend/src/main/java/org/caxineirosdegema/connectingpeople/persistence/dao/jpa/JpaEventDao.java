package org.caxineirosdegema.connectingpeople.persistence.dao.jpa;

import org.caxineirosdegema.connectingpeople.persistence.dao.EventDao;
import org.caxineirosdegema.connectingpeople.persistence.model.Event;
import org.springframework.stereotype.Repository;

@Repository
public class JpaEventDao  extends GenericJpaDao<Event> implements EventDao {

    public JpaEventDao() {
        super(Event.class);
    }
}
