package org.caxineirosdegema.connectingpeople.persistence.dao;

import org.caxineirosdegema.connectingpeople.persistence.model.User;

public interface UserDao extends Dao<User> {
}
