package org.caxineirosdegema.connectingpeople.persistence.dao.jpa;

import org.caxineirosdegema.connectingpeople.persistence.dao.UserDao;
import org.caxineirosdegema.connectingpeople.persistence.model.User;
import org.springframework.stereotype.Repository;

@Repository
public class JpaUserDao extends GenericJpaDao<User> implements UserDao {

    public JpaUserDao() {
        super(User.class);
    }
}
