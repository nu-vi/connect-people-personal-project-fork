package org.caxineirosdegema.connectingpeople.persistence.model;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "User")
public class User extends AbstractModel{

    @NotNull(message = "Name is mandatory")
    @NotBlank(message = "Name is mandatory")
    @Size(min = 3, max = 64)
    private String name;

    @NotNull(message = "Password is mandatory")
    @NotBlank(message = "Password is mandatory")
    @Size(min = 6, max = 48)
    private String password;

    @Email
    @NotBlank(message = "Email is mandatory")
    private String email;

    @Pattern(regexp = "^\\+?[0-9]*$", message = "Phone number contains invalid characters")
    @Size(min = 9, max = 16)
    private String phone;

    @NotNull(message = "City is mandatory")
    @NotBlank(message = "City is mandatory")
    @Size(min = 2, max = 64)
    private String city;

    @ManyToMany(cascade={CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinTable(name="User_Friend",
            joinColumns={@JoinColumn(name="id")},
            inverseJoinColumns={@JoinColumn(name="Friend_ID")})
    private Set<User> friendsList = new HashSet<>();

    @OneToMany(
            cascade = {CascadeType.ALL},
            orphanRemoval = true,
            mappedBy = "owner",
            fetch = FetchType.EAGER
    )
    private Set<Event> ownEventList = new HashSet<>();

    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinTable(name="User_Event",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = @JoinColumn(name = "event_id")
    )
    private Set<Event> eventList = new HashSet<>();

    public void addEvent(Event event) {

        event.setOwner(this);
        ownEventList.add(event);

        for(User friend : friendsList) {
            friend.getEventList().add(event);
            event.getInvitedUsers().add(friend);
        }

    }

    public void addFriend(User friend) {
        friendsList.add(friend);
        friend.getFriendsList().add(this);

        for(Event event : friend.getOwnEventList()) {
            this.getEventList().add(event);
        }

        for(Event e : ownEventList) {
            friend.getEventList().add(e);
        }
    }

    public void removeEvent(Event event) {
        ownEventList.remove(event);
        event.setOwner(null);
    }

    public void removeFriend(User user) {
        friendsList.remove(user);
        user.getFriendsList().remove(this);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<User> getFriendsList() {
        return friendsList;
    }

    public void setFriendsList(Set<User> friendsList) {
        this.friendsList = friendsList;
    }

    public Set<Event> getOwnEventList() {
        return ownEventList;
    }

    public void setOwnEventList(Set<Event> eventSet) {
        this.ownEventList = eventSet;
    }

    public Set<Event> getEventList() {
        return eventList;
    }

    public void setEventList(Set<Event> eventList) {
        this.eventList = eventList;
    }
}
