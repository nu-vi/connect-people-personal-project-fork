package org.caxineirosdegema.connectingpeople.persistence.dao;

import org.caxineirosdegema.connectingpeople.persistence.model.Event;

public interface EventDao extends Dao<Event> {
}
