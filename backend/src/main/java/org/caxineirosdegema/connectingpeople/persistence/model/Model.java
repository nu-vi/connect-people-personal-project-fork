package org.caxineirosdegema.connectingpeople.persistence.model;

//Common interface for a model, provides methods to set and et IDs

public interface Model {


    Integer getId();

    void setId(Integer id);
}
