
$(document).ready(function () {

    $("#submit-form").click(function (e) {

        e.preventDefault();

        $.ajax({
            url: 'http://localhost:8080/connectpeople/api/user/' + localStorage.getItem("id") + '/create-event',
            type: "POST",
            data: JSON.stringify({
                title: $("#inputPlan").val(),
                location: $("#inputPlace").val(),
                day: $("#inputDay").val(),
                month: $("#inputMonth").val(),
                date: $("#inputDate").val(),
                time: $("#inputTime").val(),
                numberOfPeople: $("#inputAmount").val()
            }),
            contentType: "application/json",
            async: true,
            success: successCallback,
            error: errorCallback
        });

    })

    document.getElementById('inputDate').min = new Date(new Date().getTime() - new Date().getTimezoneOffset() * 60000).toISOString().split("T")[0];

});

    function successCallback(response) {
        window.location.href = "/feed-page.html";
    }

    function errorCallback(request, status, error) {
        console.log(error);
    }